
#Given the input list of button numbers pressed button_pushes,
# complete the function calculate_playlist to figure out the final state of the playlist
# and return the episode labels in a list.
# Button 1: swaps the first two songs of the playlist
# Button 2: moves the first song to the end of the playlist
# Button 3: moves the last song of the playlist to the start of the playlist
# Constraint: 0 <= len(button_pushes) <= 100

def calculate_playlist(button_pushes):
    output = ["A", "B", "C", "D", "E"]
    i = 0
    while i < len(button_pushes):
        if button_pushes[i] == 1:
            output[0], output[1] = output[1], output[0]
            i += 1
        elif button_pushes[i] == 2:
            output.append(output[0])
            output.remove(output[0])
            i += 1
        elif button_pushes[i] == 3:
            output = output[-1:] + output[:-1]
            i += 1
    return output

#resource: https://www.geeksforgeeks.org/python-shift-last-element-to-first-position-in-list/

#Given a string value favorite_color and a list of candy pieces by color name (["red", "blue", ...]),
# complete the function nia_eat to return the time, in seconds, that it takes someone to eat the candy
# using Nia's strategy.

def nia_eat(favorite_color, candy):
    i = 0
    while i < len(candy):
        fave_candy = candy.count(favorite_color)
        i += 1
        total_time = (fave_candy * 23) + 17
    return total_time
