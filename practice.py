#Given the integer num_a, complete the function yas so that
# it returns the word "Yas!" with that number of "a" letters in it.
#Constraint: 1 <= num_a <= 30.

# def yas(num_a):
#     s = ""
#     beg = "Y"
#     end = "s!"
#     if num_a >= 1 and num_a <= 30:
#         n_a = num_a * "a"
#         s = n_a
#         if s[-1] == "a":
#             s += end
#             new_s = "".join((beg, s))
#     return new_s

#resource: https://www.geeksforgeeks.org/python-add-one-string-to-another/

#Given the integer num_fars, complete the function new_hope so that it returns the sentence, "A long time ago in a galaxy far, far away"
# with num_fars occurrences of the word "far".
# There should be a comma right after each "far" except for the last one.
# Constraint: 1 <= num_fars <= 6.

# def new_hope(num_fars):
#     new_s = ""
#     beg_s = "A long time ago in a galaxy"
#     end_s = " away..."
#     if num_fars == 1:
#         num_f = num_fars * " far"
#         new_s = "".join((beg_s, num_f, end_s))
#     elif num_fars > 1 and num_fars <= 6:
#         num_f = num_fars * " far,"
#         new_s = "".join((beg_s, num_f, end_s))
#         new_s = "".join((new_s.rsplit(",", 1)))
#     return new_s

#resource: https://bobbyhadz.com/blog/python-remove-last-comma-from-string

#Given the integers youngest_age and middle_age, complete the function calculate_eldest_age
# so that it returns the age of the oldest sibling.
# Constraints:
# 0 <= youngest_age <= 50
# youngest_age <= middle_age <= 50

# def calculate_eldest_age(youngest_age, middle_age):
#     if youngest_age >= 0 and youngest_age <= 50 and middle_age <= 50:
#         age_diff = middle_age - youngest_age
#         eldest_age = middle_age + age_diff
#         return eldest_age

#Given the value celsius, complete the function calculate_fahrenheit
#so that it returns the corresponding temperature in Fahrenheit.
#Constraint: -40 <= celsius <= 40

# def calculate_fahrenheit(celsius):
#     if celsius >= -40 and celsius <= 40:
#         f = (celsius * 1.8) + 32
#         return f

#Given the following input values:
# num_cupcakes, which is how many cupcakes you have
# num_tubes_frosting, which is how many tubes of frosting you have
# tubes_per_cupcake, which is exactly how many tubes of frosting go on each cupcake
# Complete the function profit so that it returns the total dollars that you make given the following information:
# A frosted cupcake sells for $10
# A cupcake with no frosting sells for $4
# A tube of frosting by itself sells for $1
# Constraints:
# 1 <= num_cupcakes <= 100
# 10 <= num_tubes_frosting <= 50
# 1 <= tubes_per_cupcake <= 5
# Example:
# Let's say you had 10 cupcakes, 5 tubes of frosting, and it takes 2 tubes of frosting to frost a cupcake.
# You can frost two cupcakes. You have one tube of frosting left over.

# def profit(num_cupcakes, num_tubes_frosting, tubes_per_cupcake):
#     frosted = num_tubes_frosting // tubes_per_cupcake
#     unfrosted = num_cupcakes - frosted
#     leftover = num_tubes_frosting % tubes_per_cupcake
#     total = (frosted * 10) + (unfrosted * 4) + (leftover * 1)
#     return total

#resource: https://www.geeksforgeeks.org/python-operators/

# The input variable message contains the string that someone wants to send in your app. Complete the function count_words to count the number of words in the string. If there are more than 30 words, then return False. Otherwise, return True.
# A "word" is considered something that's not a space.
# If you have multiple spaces in a row, then it counts as one space.
# Spaces at the beginning of the message and the end of the message don't count.
# Constraint: The message will always have at least one word.

# def count_words(message):
#     count = len(message.split())
#     if count > 30:
#         return False
#     else:
#         return True

#resource: https://www.geeksforgeeks.org/python-program-to-count-words-in-a-sentence/
