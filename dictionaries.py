# Given a list of numbers numbers, complete the function mode that returns
# a list of all of the modes of the list of numbers.

def mode(numbers):
    count_dict = (dict((x, numbers.count(x)) for x in set(numbers)))
    mode_nums = [k for k, v in count_dict.items() if v == max(count_dict.values())]
    return mode_nums

#resource: https://www.geeksforgeeks.org/python-list-count-method/
# https://stackoverflow.com/questions/47861737/how-to-get-multiple-max-key-values-in-a-dictionary


#Given a list registrants that contains the unique names of everyone who runs the race,
#  and a list finishers that contains the names of all of the registrants who completed the race,
#  complete the function find_missing_registrant to return the name of the one person who registered
#  for the race and did not complete it.
# Constraints: len(finishers) = len(registrants) - 1

def find_missing_registrant(registrants, finishers):
    return "".join(set(registrants).difference(finishers))

#resources: https://www.geeksforgeeks.org/python-find-missing-additional-values-two-lists/#:~:text=Approach%3A%20To%20find%20the%20missing,difference%20of%20list2%20from%20list1.
#https://www.geeksforgeeks.org/convert-set-to-string-in-python/
