#Given the game_length in minutes and two lists team_1_score_times and team_2_score_times,
# complete the function half_time_scores to determine the combined total number of times both teams
# scored in the first half.

def half_time_scores(
    game_length, #in minutes
    team_1_score_times, #in seconds
    team_2_score_times #in seconds
):
    game_length_second = game_length * 60
    half_time_second = game_length_second / 2
    team_1_half = 0
    team_2_half = 0
    for score in team_1_score_times:
        if score < half_time_second:
            team_1_half += 1
    for scores in team_2_score_times:
        if scores < half_time_second:
            team_2_half += 1
    total_half = team_1_half + team_2_half
    return total_half
