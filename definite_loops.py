# Given the input string swaps and that the Queen always starts in the middle,
# complete the function monty so that it returns where the Queen is
# after all of the swaps are done: left, middle, or right.
# The string swaps contains the letters L, R, and O. This is what each means:
# L means swap the left and middle cards
# R means swap the right and middle cards
# O means swap the left and right cards, the outside cards
# Constraints:
# 0 <= swaps <= 50
# swaps will have only the letters L, R, and O in it
# Example:
# Let's say that swaps is "LOLROL". Then, the following table shows the position of the Queen for each letter:

def monty(swaps):
    swaps_list = ["middle"]
    queen_position = ["left", "middle", "right"]
    for swap in swaps:
        if swap == "L" and swaps_list[-1] == "middle":
            swaps_list.append(queen_position[0])
        elif swap == "L" and swaps_list[-1] == "right":
            swaps_list.append(queen_position[2])
        elif swap == "L" and swaps_list[-1] == "left":
            swaps_list.append(queen_position[1])
        elif swap == "R" and swaps_list[-1] == "middle":
            swaps_list.append(queen_position[2])
        elif swap == "R" and swaps_list[-1] == "right":
            swaps_list.append(queen_position[1])
        elif swap == "R" and swaps_list[-1] == "left":
            swaps_list.append(queen_position[0])
        elif swap == "O" and swaps_list[-1] == "left":
            swaps_list.append(queen_position[2])
        elif swap == "O" and swaps_list[-1] == "right":
            swaps_list.append(queen_position[0])
        elif swap == "O" and swaps_list[-1] == "middle":
            swaps_list.append(queen_position[1])
    return swaps_list[-1]
#Given two strings of up to 100 characters each, yesterday and today,
# complete the function num_same_spaces to return the number of parking spaces that were occupied both
# yesterday and today.
#Constraints:
#1 <= len(yesterday) <= 100
# len(yesterday) == len(today)
# Example:
# Here are two strings that you need to compare.
# Underneath the two strings are stars that show where the same parking space was filled on both mornings.

def num_same_spaces(yesterday, today):
    count = 0
    for i in range(0, len(yesterday)):
        if yesterday[i] == "C" and today[i] == "C":
            count += 1
    return count

#resources: https://stackoverflow.com/questions/43638769/algorithms-for-counting-identical-characters-in-the-same-position-between-two-st

#Given a list of how much Paz spends each month, expenses, complete the function cash_on_hand
# that will return the amount of money she'll have the month after all of the expenses.
# Constraints: 1 <= len(expenses) <= 100
# Example:
# Paz's expenses for five months are [10, 20, 30, 40, 45].
# Your function cash_on_hand would return 35 in this scenario.

def cash_on_hand(expenses):
    current_cash = 30
    for expense in expenses:
        current_cash += 30 - expense
    return current_cash

#Given an input string password, complete the function valid_password
# to return the string good if it's a valid password, or to return the string bad if it's an invalid password.
# The criteria for the password are:
# It must be have 8, 9, 10, 11, or 12 characters
# It must have at least two lowercase letters (a … z)
# It must have at least three uppercase letters (A … Z)
# It must have at least two digits (0 … 9)

def valid_password(password):
    lower_count = 0
    upper_count = 0
    num_count = 0
    for char in password:
        if char.islower():
            lower_count += 1
        if char.isupper():
            upper_count += 1
        if char.isdigit():
            num_count += 1
    if (
        len(password) in range(8, 13) and
        lower_count >= 2 and
        upper_count >= 3 and
        num_count >= 2
    ):
        return "good"
    else:
        return "bad"

#Given two strings, submission and answer_key, complete the function grade_scantron
# to compare the two strings and return the number of correct answers the student
# received in their submission based on the value of the answer_key.
#A correct answer occurs when the characters in both the submission and the
# answer_key at the same position are the same letters.
#Constraints:
# submission and answer_key will be the same length
# answer_key will contain only the letters "A", "B", "C", "D", or "E"
# submission will contain only the characters "A", "B", "C", "D", "E", or a space " "
# 0 <= answer_key <= 10000

def grade_scantron(submission, answer_key):
    score = 0
    for i in range(0, len(submission)):
        if submission[i] == answer_key[i]:
            score += 1
    return score

#Given a string input text that contains some text in English or German,
# complete the function language so that it returns "German", "English", or "Maybe French?"
# given the following conditions:
# If the text has more "ei" than "ie" sequences in it, then respond with "German"
# If the text has more "ie" than "ei" sequences in it, then respond with "English"
# If the text has an equal number of "ie" and "ei" sequences in it, then respond with "Maybe French?"

def language(text):
    german = text.count("ei")
    english = text.count("ie")
    if german > english:
        return "German"
    elif english < german:
        return "English"
    elif english == german:
        return "Maybe French?"
