#Given the chosen number for each of the courses from the above menu items, complete the function calories to return the total calories for the choices.

# If someone chooses the value 0, then that means they're going to skip that part of the meal.

# Constraints: All inputs will be greater than or equal to 0 and less than or equal to 3.

# Example:

# If someone chooses:

# 2 for their entree (veggie burger: 399 calories)
# 3 for their side (salad: 72 calories)
# 0 for their dessert (no dessert: 0 calories)
# 3 for their drink (martini: 120 calories)
# then the total calories for that meal is 591 calories.

def calories(entree_num, side_num, dessert_num, drink_num):
    entree = [522, 399, 501]
    side = [130, 125, 72]
    dessert = [222, 391, 100]
    drink = [10, 8, 120]
    total = 0
    if entree_num == 0:
        total + 0
    elif entree_num == 1:
        total += entree[0]
    elif entree_num == 2:
        total += entree[1]
    elif entree_num == 3:
        total += entree[2]

    if side_num == 0:
        total + 0
    elif side_num == 1:
        total += side[0]
    elif side_num == 2:
        total += side[1]
    elif side_num == 3:
        total += side[2]

    if dessert_num == 0:
        total + 0
    elif dessert_num == 1:
        total += dessert[0]
    elif dessert_num == 2:
        total += dessert[1]
    elif dessert_num == 3:
        total += dessert[2]

    if drink_num == 0:
        total + 0
    elif drink_num == 1:
        total += drink[0]
    elif drink_num == 2:
        total += drink[1]
    elif drink_num == 3:
        total += drink[2]

    return total

#Given the number of a month month_num (1 - 12) and
# the number of the day day_num (1 - 31, as appropriate),
# complete the function skating_day to return the appropriate message.

def skating_day_message(month_num, day_num):
    if month_num <= 12 and day_num < 4 or month_num < 12 and day_num <= 31:
        return "World Ice Skating Day is coming up!"
    elif month_num == 12 and day_num == 4:
        return "YAY! It's World Ice Skating Day!"
    elif month_num == 12 and day_num > 4:
        return "You just missed it. There's another next year!"


#Given the input message, complete the function sentiment to return one of four messages:
# Return none if the message does not contain any emoticons
# Return unsure if the message contains an equal number of happy and sad emoticons
# Return happy if the message contains more happy emoticons than sad emoticons
# Return sad if the message contains more sad emoticons than happy emoticons
# Constraints: 1 <= message length <= 255

def sentiment(message):
    happy_count = message.count(":-)")
    sad_count = message.count(":-(")

    if happy_count == 0 and sad_count == 0:
        return "none"
    elif happy_count == sad_count:
        return "unsure"
    elif happy_count > sad_count:
        return "happy"
    elif happy_count < sad_count:
        return "sad"

#resource: https://www.geeksforgeeks.org/python-count-occurrences-of-a-character-in-string/

#Given the inputs pizza_size and cheese_multiplier, complete the function pizza_satisfaction
# to return Nia's satisfaction rating based on those values.
# Constraints:
# pizza_size is the size of the pizza in inches
# cheese_multiplier is 1, 2, or 3 depending on if there's normal cheese,
# double cheese, or triple cheese, respectively

def pizza_satisfaction(pizza_size, cheese_multiplier):
    if pizza_size >= 12 and cheese_multiplier == 3:
        return "maximally satisfied"
    elif pizza_size >= 10 and (cheese_multiplier == 3 or cheese_multiplier == 2):
        return "really satisfied"
    else:
        return "very satisfied"


#Given a phone number as a ten digit number (1234567890, for example), complete the function scammer to return the message ignore if the number satisfies all four of the following properties:
# The first digit is a 2 or 4
# The fifth digit is a 2 or 4
# The seventh and eight digits are the same
# The last digit is a 0
# If the phone number does not satisfy all four of those properties, then return accept.

def scammer(number):
    num_list = [int(x) for x in str(number)]
    if (
        (num_list[0] == 2 or num_list[0] == 4) and
        (num_list[4] == 2 or num_list[4] == 4) and
        (num_list[6] == num_list[7]) and
        (num_list[9] == 0)
    ):
        return "ignore"
    else:
        return "accept"

#resource: https://www.geeksforgeeks.org/python-convert-number-to-list-of-integers/
